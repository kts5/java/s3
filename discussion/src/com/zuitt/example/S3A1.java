package com.zuitt.example;

import java.util.Scanner;

public class S3A1 {
    public static void main(String[] args){

        Scanner input = new Scanner (System.in);

        int factorial = 1;
        int i = 1;

        System.out.println("Input an integer whose factorial will be computed :");

        int num = input.nextInt();

        while(i <= num){
            factorial = factorial * i;
            i++;
        }
        System.out.println("The factorial of " + num + " is " + factorial);

    }
}
